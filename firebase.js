import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyBel_T9Aa2ssiwBJEDVvdjxM3tvRl0j7_g",
  authDomain: "restaurante-802b7.firebaseapp.com",
  databaseURL: "https://restaurante-802b7.firebaseio.com",
  projectId: "restaurante-802b7",
  storageBucket: "restaurante-802b7.appspot.com",
  messagingSenderId: "438719402465",
  appId: "1:438719402465:web:75759bca44aeb2653142a0",
};

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore()
const auth = firebase.auth()
const Timestamp = firebase.firestore.Timestamp;

export { firebase, db, auth, Timestamp }

import Vue from "vue";
import Vuex from "vuex";
import router from "../router";
import _ from "lodash";
import { auth, db } from "../../firebase";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    usuario: null,
    error: null,
    clientes: [],
    ventas: [],
    productos: [],
    inventario: [],
    planilla: [],
  },
  mutations: {
    setUsuario(state, payload) {
      state.usuario = payload;
    },
    setError(state, payload) {
      state.error = payload;
    },
    setVentas(state, payload) {
      state.ventas = payload;
    },
    setClientes(state, payload) {
      state.clientes = payload;
    },
    setProductos(state, payload) {
      state.productos = payload;
    },
    setInventario(state, payload) {
      state.inventario = payload;
    },
    setPlanilla(state, payload) {
      state.planilla = payload;
    },
  },
  actions: {
    async ingresoUsuario({ commit }, usuario) {
      try {
        let data = null;
        const dbref = db.collection("user");

        const snapshot = await dbref.where("username", "==", usuario.username).get();

        snapshot.forEach(doc => {
          data = doc.data();
        });
        if (data === null) {
          commit("setError", 'auth/user-not-found');
          return
        }
        if (data.role !== 'ADMIN') {
          commit('setError', 'auth/admin-permission')
          return;
        }

        const res = await auth.signInWithEmailAndPassword(
          data.email,
          usuario.password
        );

        const usuarioLogeado = {
          nombre: data.nombre,
          email: data.email,
          name: data.username,
          uid: data.uid,
          role: data.role,
        };
        commit("setUsuario", usuarioLogeado);
        router.push({ name: 'Dashboard', params: { id: 1 } })
      } catch (error) {
        commit("setError", error.code);
      }
    },
    cerrarSesion({ commit }) {
      auth.signOut().then(() => {
        router.push("/login");
      });
    },
    async detectarUsuario({ commit }, usuario) {
      if (usuario !== null) {
        try {
          const res = await db
            .collection("user")
            .doc(usuario.uid)
            .get();

          const usuarioLogeado = {
            nombre: res.data().nombre,
            email: usuario.email,
            name: res.data().username,
            uid: res.data().uid,
            role: res.data().role,
          };
          commit("setUsuario", usuarioLogeado);
        } catch (error) {
          console.log(error)
        }
      } else {
        commit('setUsuario', null);
      }
    },
    getVentas({ commit }, params) {
      let fecha = params.restaurante === 'fogoncito1' ? 'fecha_venta' : 'fecha'
      const ref = db.collection(params.restaurante).doc('ventas').collection('ventas');
      ref.where(fecha, '>', params.startDate).where(fecha, '<', params.endDate).get().then((querySnapshot) => {
          let ventas = [];
          let clientes = []
          if (querySnapshot.empty) {
            console.log("No matching documents. Empty");
            commit("setClientes", clientes);
            commit("setVentas", ventas);
            return;
          }

          querySnapshot.forEach((doc) => {
            let venta = doc.data();
            venta.id = doc.id;
            venta.show = false,
              ventas.push(venta);
          });

          ventas.forEach((venta) => {
            let cliente = {
              nombre: venta.cliente.nombre,
              cedula: venta.cliente.cedula,
              telefono: venta.cliente.telefono,
              direccion: venta.cliente.direccion
            };
            clientes.push(cliente);

          });

          commit("setClientes", clientes);
          commit("setVentas", ventas);
        });
    },
    async editVentas({ commit }, params) {
      const ref = db.collection(params.restaurante).doc('ventas').collection('ventas').doc(params.id);
      const res = await ref.update({
        pedidos: params.pedidos,
      });
      try {
        console.log(`Document ${res.id} has been updated.`)
      } catch (error) {
        console.log("No matching documents.\nError: ", error);
      }

    },
    getInventario({ commit }, restaurante) {
      db.collection(restaurante).doc('inventario').collection('inventario')
        .onSnapshot((querySnapshot) => {
          let inventario = [];
          if (querySnapshot.empty) {
            console.log("No matching documents. Empty");
            commit("setInventario", inventario);
            return;
          }

          querySnapshot.forEach((doc) => {
            let item = doc.data();
            item.id = doc.id;
            inventario.push(item);
          });

          commit("setInventario", inventario);
        });
    },
    async addInventario({ commit }, item) {
      try {
        const ref = db.collection(item.username).doc("inventario").collection("inventario");
        const res = await ref.add({
          index: item.index,
          nombre: item.nombre,
          productos: item.productos,
          relacion: item.relacion,
          vendidos: item.vendidos,
          /*fecha: item.fecha,
          cantidad: item.cantidad,*/
        });

        console.log(`Document ${res.id} has been updated.`)
      } catch (error) {
        console.log("No matching documents.\nError: ", error);
      }
    },
    async addInventarioToProduct({commit}, item) {

    },
    async editInventario({ commit }, item) {
      try {
        const ref = db.collection(item.username).doc("inventario").collection("inventario").doc(item.id);
        const res = await ref.update({
          cantidad: item.cantidad,
          nombre: item.nombre,
          productos: item.productos,
          relacion: item.relacion,
        });

        console.log(`Document ${res.id} has been updated.`)
      } catch (error) {
        console.log("No matching documents.\nError: ", error);
      }
    },
    async deleteInventario({ commit }, item) {
      try {
        const ref = db.collection(item.username).doc("inventario").collection("inventario").doc(item.id);
        const res = await ref.delete();

        console.log(`Document ${res.id} has been deleted.`)
      } catch (error) {
        console.log("No matching documents.\nError: ", error);
      }
    },
    async getProductos({ commit, state }, restaurante) {
      let productos = [];

      try {
        if (state.ventas !== null) {
          const ref = db.collection(restaurante).doc('productos');
          const item = await ref.get();
          if (item.empty) {
            console.log("No matching documents. Empty");
            return;
          }
          productos = item.data().productos;
        }
      } catch (error) {
        console.log("No matching documents. Error: ", error);
      }
      commit("setProductos", productos);
    },
    async getPlanilla({ commit, state }) {
      try {
        db.collection('planilla')
          .onSnapshot((querySnapshot) => {
            let planilla = [];
            if (querySnapshot.empty) {
              console.log("No matching documents. Empty");
              commit("setPlanilla", planilla);
              return;
            }

            querySnapshot.forEach((doc) => {
              let item = doc.data();
              item.id = doc.id;
              planilla.push(item);
            });

            commit("setPlanilla", planilla);
          });
      } catch (error) {
        console.log("No matching documents. Error: ", error);
      }
    },
    async addEmployee({ commit }, item) {
      try {
        const ref = db.collection('planilla');
        const res = await ref.add({
          nombre: item.nombre,
          apellidos: item.apellidos,
          fecha_ingreso: item.fecha_ingreso,
          fecha_final: null,
        });

        console.log(`Document ${res.id} has been updated.`)
      } catch (error) {
        commit("setError", `No se pudo ejecutar la acción.\nError: ${error}`);
        setTimeout(()=> {commit("setError",null)}, 3500);
      }
    },
    async editEmployee({ commit }, item) {
      try {
        const ref = db.collection('planilla').doc(item.id);
        const res = await ref.update({
          nombre: item.nombre,
          apellidos: item.apellidos,
          fecha_ingreso: item.fecha_ingreso,
          fecha_final: item.fecha_final,
        });

        console.log(`Document ${res.id} has been updated.`)
      } catch (error) {
        commit("setError", `No se pudo ejecutar la acción.\nError: ${error}`);
      }
    },
  },
  getters: {
    existeUsuario(state) {
      if (state.usuario === null) {
        return false;
      } else {
        return true;
      }
    },
  },
  modules: {},
});

import Clientes from "./Tables/Clientes.vue";
import Productos from "./Tables/Productos.vue";
import Inventario from "./Tables/Inventario.vue";
import StatsCard from "./Cards/StatsCard.vue"
import Detalle from './Tables/Detalle.vue'

export {
  StatsCard,
  Clientes,
  Productos,
  Inventario,
  Detalle,
};
